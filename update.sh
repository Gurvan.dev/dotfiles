#!/bin/sh
# Author : gurvan.dev <https://gitlab.com/gurvan.dev>

OLD_DIR=$PWD
BASE_DIR=$(dirname "$0")

cd $BASE_DIR
git pull
./install_packages.sh
./setup.sh
cd $OLD_DIR
