#!/bin/sh
# Author : gurvan.dev <https://gitlab.com/gurvan.dev>

BASEDIR=$(dirname "$0")
PKGSDIR="${BASEDIR}/pkgs"

# System wide packages ---------------------------------------------------------

if [ $(command -v pacman) ]; then
  sudo pacman -Syu --needed $(cat $PKGSDIR/arch)
fi

if [ $(command -v yay) ]; then
  yay -Syu --needed $(cat $PKGSDIR/aur)
fi

if [ $(command -v dnf) ]; then
  sudo dnf install $(cat $PKGSDIR/dnf)
fi

# Language-Level packages ------------------------------------------------------

if [ $(command -v opam) ]; then
  opam init -a
  opam update
  opam upgrade
  opam install $(cat $PKGSDIR/opam)
  opam user-setup install
fi

