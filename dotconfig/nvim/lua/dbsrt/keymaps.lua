local function add_keymap (mode, lhs, rhs, description)
  local keymap = vim.api.nvim_set_keymap
  keymap(mode, lhs, rhs, { noremap = true, silent = true, desc = description })
end

-- Space as leader key
add_keymap("", "<Space>", "<Nop>")
vim.g.mapleader = " "
vim.g.maplocalleader = " "

-- Modes
-- Normal       "n"
-- Insert       "i"
-- Visual       "v"
-- Visual Block "x"
-- Term         "t"
-- Command      "c"

-- normal mode -----------------------------------------------------------------

-- Navigation between windows: use Ctrl+Dir instead of Ctrl+w+Dir
add_keymap("n", "<C-h>", "<C-w>h", "Navigate to left buffer")
add_keymap("n", "<C-j>", "<C-w>j", "Navigate to bottom buffer")
add_keymap("n", "<C-k>", "<C-w>k", "Navigate to top buffer")
add_keymap("n", "<C-l>", "<C-w>l", "Navigate to right buffer")

-- -- Open tree with Leader+e
-- add_keymap("n", "<leader>e", ":NvimTreeToggle<CR>", "Open tree")
--
-- -- Open terminal with Leader+;
-- add_keymap("n", "<leader>;", ":ToggleTerm direction=horizontal<CR>", "Open terminal")

-- Split with leader key
add_keymap("n", "<leader>l", ":vsplit<CR>", "Vertical split")
add_keymap("n", "<leader>j", ":split<CR>", "Split")

-- Resize window with arrow
add_keymap("n", "<C-Up>", ":resize +2<CR>")
add_keymap("n", "<C-Down>", ":resize -2<CR>")
add_keymap("n", "<C-Left>", ":vertical resize +2<CR>")
add_keymap("n", "<C-Right>", ":vertical resize -2<CR>")

-- Navigate between buffer with Shift+L and Shift+h
add_keymap("n", "<S-l>", ":bnext<CR>", "Next buffer")
add_keymap("n", "<S-h>", ":bprevious<CR>", "Previous buffer")

-- Telescope
add_keymap("n", "<leader>f", ":Telescope live_grep<CR>", "Telescope search")

-- Deux <esc> pour quitter la recherche
add_keymap("n", "<Esc><Esc>", ":nohl<CR>")

add_keymap("n", "²", ":set invrnu", "Toggle relative number")

-- insert mode -----------------------------------------------------------------

add_keymap("i", "<C-j>", "<C-o>j", "")
add_keymap("i", "<C-k>", "<C-o>k", "")

-- visual mode -----------------------------------------------------------------

-- Stay in indent mode
add_keymap("v", "<", "<gv")
add_keymap("v", ">", ">gv")

-- Move text up and down with alt j and akt k
add_keymap("v", "<A-j>", ":m .+1<CR>==")
add_keymap("v", "<A-k>", ":m .-1<CR>==")

-- Keep paste
add_keymap("v", "p", '"_dP')

-- terminal --------------------------------------------------------------------

-- Easily escape terminal mode
add_keymap("t", "<Esc>", "<C-\\><C-n>", "Quit terminal mode")
