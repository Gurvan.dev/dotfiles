local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

-- Lazy require setting up leader key before plugin loading
local opts = { noremap = true, silent = true }
vim.api.nvim_set_keymap("", "<Space>", "<Nop>", opts)
vim.g.mapleader = " "
vim.g.maplocalleader = " "

require('lazy').setup({

  "nvim-lua/popup.nvim",        -- An implementation of the Popup API from vim in Neovim
  "nvim-lua/plenary.nvim",      -- Useful lua functions used by lots of plugins
  "EdenEast/nightfox.nvim",     -- Colorscheme
  "gurvan-dev/jasmin.nvim",     -- Second colorscheme

  -- Completions plugins
  "hrsh7th/nvim-cmp",           -- Completion plugin
  "hrsh7th/cmp-buffer",         -- Buffer completion
  "hrsh7th/cmp-path",           -- Path completion
  "hrsh7th/cmp-cmdline",        -- Cmdline completion
  "saadparwaiz1/cmp_luasnip",   -- Snippet completion
  "hrsh7th/cmp-nvim-lsp",       -- Completion from lsp

  -- Snippets
  "L3MON4D3/LuaSnip",       -- Snippet Engine, Necessary for completion

  -- LSP
  "neovim/nvim-lspconfig",  -- Provide default lsp configuration

  -- Comment out code quickly
  "numToStr/Comment.nvim",
  "machakann/vim-sandwich",

  -- GitSigns
  "lewis6991/gitsigns.nvim",

  -- File explorer
  "nvim-tree/nvim-tree.lua",
  "nvim-tree/nvim-web-devicons",

  -- Buffer line & statusline
  "ojroques/nvim-hardline",

  -- Telescope & Dépendances
  "nvim-telescope/telescope-fzf-native.nvim",
  "BurntSushi/ripgrep",
  { "nvim-telescope/telescope.nvim", tag = '0.1.2' },

  -- Which Key
  "folke/which-key.nvim",

  -- LateX Support
  "lervag/vimtex",
  "peterbjorgensen/sved",

  -- Coq
  "whonore/Coqtail",

  -- Meilleur terminal
  { "akinsho/toggleterm.nvim", version = "*", config = true},

  -- Highlight colors
  "lilydjwg/colorizer",

  -- Markdown
  { "ellisonleao/glow.nvim", config = true, cmd = "Glow"}
})

