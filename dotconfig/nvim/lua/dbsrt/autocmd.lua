-- Syntax highlighting ---------------------------------------------------------

-- Associate file type to syntax
local fts = {
  cub   = "cubicle",
  imp   = "imp",
  simp  = "simp",
  lus   = "lustre",
  ept   = "lustre",
  obj   = "objlng",
}

local options_group = vim.api.nvim_create_augroup("OptionsGroup", {clear = true})

vim.api.nvim_create_autocmd({"FileType","BufRead","BufNewFile"}, {
  pattern = {"*.tex", "*.md"},
  callback = function()
    vim.cmd("setlocal spell")
  end,
  group = options_group
})

for k, v in pairs(fts) do
  vim.api.nvim_create_autocmd({"BufRead","BufNewFile"},
    {
      pattern = "*." .. k,
      command = "set filetype=" .. v,
      group = options_group,
    }
  )
end

-- Reformat --------------------------------------------------------------------

-- Remove trailing white space and retab on buffer save
vim.api.nvim_create_autocmd({"BufWritePre"}, {
  callback = function ()
    vim.cmd("retab")
    local save_cursor = vim.fn.getpos(".")
    vim.cmd([[%s/\s\+$//e]])
    vim.fn.setpos(".", save_cursor)
  end,
})
