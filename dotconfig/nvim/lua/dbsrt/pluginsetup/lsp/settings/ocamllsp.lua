vim.opt.rtp:append(vim.fn.system {'opam', 'var', 'share'}:gsub("\n", "") .. "/merlin/vim")

local opts = {
  cmd = { "ocamllsp", "--fallback-read-dot-merlin" },
}
return opts
