-- diagnostic style ------------------------------------------------------------

local diagnostic_config = {
  virtual_text = true,
  signs = {
    active = signs,
  },
  update_in_insert = true,
  underline = true,
  severity_sort = true,
  float = {
    focusable = false,
    style  = "minimal",
    border = "rounded",
    source = "always",
    header = "",
    prefix = "",
  },
}

-- keymaps ---------------------------------------------------------------------

local function lsp_keymaps(client, bufnr)
  local opts = { noremap = true, silent = true }
  local keymap = vim.api.nvim_buf_set_keymap
  -- todo : Better keymap, descriptions
  keymap(bufnr, "n", "gD", "<cmd>lua vim.lsp.buf.declaration()<CR>", opts)
  keymap(bufnr, "n", "gd", "<cmd>lua vim.lsp.buf.definition()<CR>", opts)
  keymap(bufnr, "n", "K", "<cmd>lua vim.lsp.buf.hover()<CR>", opts)
  keymap(bufnr, "n", "gi", "<cmd>lua vim.lsp.buf.implementation()<CR>", opts)

  keymap(bufnr, "n", "gr", "<cmd>lua vim.lsp.buf.references()<CR>", opts)
  keymap(
    bufnr,
    "n",
    "gl",
    '<cmd>lua vim.diagnostic.open_float(0, {scope="line", border="rounded"})<CR>',
    opts
  )

  keymap(bufnr, "n", "<leader>q", "<cmd>lua vim.diagnostic.setloclist()<CR>", opts)
  vim.cmd [[ command! Format execute 'lua vim.lsp.buf.formatting()' ]]
end

-- lsp config ------------------------------------------------------------------

local lspconfig = require "lspconfig"
local servers = {
  "ocamllsp",       -- OCaml
  "clangd",         -- C
  "texlab",         -- LateX
  "pyright",        -- Python
  "rust_analyzer",  -- Rust
  "gopls",          -- Go
  "zls",            -- Zig
  "lua_ls",         -- Lua
}

local on_attach = lsp_keymaps
local setup = function()
  vim.diagnostic.config(diagnostic_config)
end

for _, server in pairs(servers) do
  local opts = {
    on_attach     = on_attach,
    setup         = setup,
  }

  server = vim.split(server, "@")[1]

  local require_ok, confopts = pcall(require, "dbsrt.pluginsetup.lsp.settings." .. server)
  if require_ok then
    opts = vim.tbl_deep_extend("force", confopts, opts)
  end

  lspconfig[server].setup(opts)

end
