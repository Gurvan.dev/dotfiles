require "nvim-tree".setup {
  disable_netrw = true,
  hijack_cursor = true,         -- Keep cursor on first letter of filename
  filters = {
    git_ignored = false,
  },
}
