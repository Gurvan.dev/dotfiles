local actives = {
  "cmp",
  "tree",
  "comment",
  "gitsigns",
  "glow",
  "hardline",
  "lsp",
  "toggleterm",
  "whichkey",
  "telescope",
}

for _, active in pairs(actives) do
  require ("dbsrt.pluginsetup." .. active)
end
