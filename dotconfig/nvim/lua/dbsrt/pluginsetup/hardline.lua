require("hardline").setup({
  bufferline = true,
  bufferline_settings = {
    exclude_terminal = true,
  },
  theme = 'jasmin',
})
