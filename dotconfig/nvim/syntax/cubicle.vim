" Vim syntax file
" Language: Cubicle
" Maintainer: Gurvan <gurvan@debauss.art>
" Last Change: 2023 Sept 21

if exists("b:current_syntax")
  finish
endif

syn clear

syn keyword cubicleType bool int real proc
syn keyword cubicleQuant forall_other exists_other forall exists
syn keyword cubicleFunction init transition unsafe predicate requires invariant
syn keyword cubicleDecl var array const type
syn keyword cubicleConditional if then else case forall forall_other

syn match cubicleNumber "\<[0-9]\+\>"
syn match cubicleFloat  "\<[0-9]\+.[0-9]\+\>"
syn keyword cubicleConstant True False
syn match cubicleOperator "&&" "||" "<=>" "=>" "not"

syn region cubicleComment start="(\*"  end="\*)"

hi def link cubicleType         Type
hi def link cubicleConstant     Number
hi def link cubicleNumber       Number
hi def link cubicleFloat        Number
hi def link cubicleComment      Comment
hi def link cubicleConditional  Conditional
hi def link cubicleFunction     Statement
hi def link cubicleDecl         Statement
hi def link cubicleQuant        Statement
hi def link cubicleOperator     Operator

let b:current_syntax = "cub"
