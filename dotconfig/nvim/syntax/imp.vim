" Vim syntax file
" Language: Imp
" Maintainer: Gurvan <gurvan@debauss.art>
" Last Change: 2023 Sept 21

if exists("b:current_syntax")
  finish
endif

syn clear

syn keyword impKeyword putchar var function return
syn keyword impConditional if else
syn keyword impRepeat while
syn keyword impBool true false
syn match   impNumber "\<[0-9]\+\>"
syn region  impComment start="/\*"  end="\*/"

hi def link impKeyword     Keyword
hi def link impConditional Conditional
hi def link impRepeat      Repeat
hi def link impBool        Boolean
hi def link impNumber      Number
hi def link impComment     Comment

let b:current_syntax = "imp"
