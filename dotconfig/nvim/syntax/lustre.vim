" Vim syntax file
" Language: Lustre
" Author: Timothy Bourke
" Last Change: 2023 Oct 02

if exists("b:current_syntax")
  finish
endif

syn clear

syn match   default '[a-zA-Z_][a-zA-Z0-9_]*'
syn match   lusStatement "=\|;"
syn keyword lusType int real bool
syn keyword lusControl if else then with
syn keyword lusNode node fun const let var returns
syn keyword lusNode automaton state switch end reset every
syn keyword lusControl do until unless continue last
syn match   lusNode "tel."
syn match   lusNode "tel"
syn keyword lusNode type const include
syn keyword lusCommands assert
syn keyword lusCommands pre fby current merge when and or not xor mod default
syn match   lusCommands "->"
syn match   lusCommands "initially"
syn keyword lusConstant true false
syn match   lusConstant "[-]\d*[\.]\d"
syn match   lusConstant "\d*[\.]\d*"

syn region Comment start="--" end="$"
syn region Comment start="(\*" end="\*)" excludenl
syn region Comment start="/\*" end="\*/" excludenl

hi def link lusStatement Statement
hi def link lusNode      Statement
hi def link lusCommands  Keyword
hi def link lusControl   Conditional
hi def link lusConstant  Constant
hi def link lusType      Type

let b:current_syntax = "lus"
