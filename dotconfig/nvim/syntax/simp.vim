" Vim syntax file
" Language: Simp
" Maintainer: Gurvan <gurvan@debauss.art>
" Last Change: 2023 Sept 21

if exists("b:current_syntax")
  finish
endif

syn clear

syn keyword simpKeyword putchar var function return struct
syn keyword simpConditional if else
syn keyword simpRepeat while
syn keyword simpBool true false
syn match   simpNumber "\<[0-9]\+\>"
syn region  simpComment start="/\*"  end="\*/"
syn keyword simpType int bool void

hi def link simpKeyword     Keyword
hi def link simpConditional Conditional
hi def link simpRepeat      Repeat
hi def link simpBool        Boolean
hi def link simpNumber      Number
hi def link simpComment     Comment
hi def link simpType        Type

let b:current_syntax = "simp"
