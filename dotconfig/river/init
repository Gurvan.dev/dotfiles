#!/bin/sh

MOD="Super"
TERMINAL_CLIENT="footclient"
TERMINAL="foot"
RUNNER="fuzzel"
FILE_EXPLORER="pcmanfm"

COLOR_PICKER="wl-color-picker clipboard"
SCREEN_CAPTURE="grim && notify-send 'Screenshot taken.'"
SCREEN_CAPTURE_PARTIAL="grim -g \"\$(slurp)\" && notify-send 'Screenshot taken.'"

### General settings

# Set to AZERTY keyboard
riverctl keyboard-layout fr

# Follow mouse
riverctl focus-follows-cursor normal

# When changing view, move cursor to view
riverctl set-cursor-warp on-focus-change

# Set background and border color
riverctl background-color       0x000000
riverctl border-color-focused   0xdfdfdf
riverctl border-color-unfocused 0x000000
riverctl border-width           2

### Keyboard bindings

## Spawning

riverctl map normal $MOD BackSpace spawn $FILE_EXPLORER

# $MOD+Return to start terminal client
riverctl map normal $MOD Return spawn $TERMINAL_CLIENT

# $MOD+Shift+Return to start terminal
riverctl map normal $MOD+Shift Return spawn $TERMINAL

# $MOD+R to run app launcher
riverctl map normal $MOD R spawn $RUNNER

# $MOD+P to run color picker
riverctl map normal $MOD+Shift P spawn "eval $COLOR_PICKER"

# $MOD+Shift+S to take screen capture
riverctl map normal $MOD+Shift S spawn "eval $SCREEN_CAPTURE"

# $MOD+S to take partial screen capture
riverctl map normal $MOD S spawn "$SCREEN_CAPTURE_PARTIAL"

## Quitting

# $MOD+Shift+C to close the focused view
riverctl map normal $MOD+Shift C close
# $MOD+Q to lock the screen
riverctl map normal $MOD Q spawn swaylock

## Moving around

# $MOD+J and $MOD+K to focus the next/previous view in the layout stack
riverctl map normal $MOD J focus-view next
riverctl map normal $MOD K focus-view previous

# $MOD+Shift+J and $MOD+Shift+K to swap the focused view with the next/previous
# view in the layout stack
riverctl map normal $MOD+Shift J swap next
riverctl map normal $MOD+Shift K swap previous

# $MOD+Period and $MOD+Comma to focus the next/previous output
riverctl map normal $MOD Semicolon focus-output next
riverctl map normal $MOD Comma focus-output previous

# $MOD+Shift+{Period,Comma} to send the focused view to the next/previous output
riverctl map normal $MOD+Shift Comma send-to-output next
riverctl map normal $MOD+Shift Period send-to-output previous

# $MOD+Z to bump the focused view to the top of the layout stack
riverctl map normal $MOD Z zoom

# $MOD+H and $MOD+L to decrease/increase the main ratio of rivertile(1)
riverctl map normal $MOD H send-layout-cmd rivertile "main-ratio -0.05"
riverctl map normal $MOD L send-layout-cmd rivertile "main-ratio +0.05"

# $MOD+Shift+H and $MOD+Shift+L to increment/decrement the main count of
# rivertile(1)
riverctl map normal $MOD+Shift H send-layout-cmd rivertile "main-count +1"
riverctl map normal $MOD+Shift L send-layout-cmd rivertile "main-count -1"

i=1
keys=("Ampersand" "Eacute" "Quotedbl" "Apostrophe" "Parenleft" "Minus" "Egrave" "Underscore" "Ccedilla")
for key in "${keys[@]}"
do
    tags=$((1 << ($i - 1)))

    # $MOD+[1-9] to focus tag [0-8]
    riverctl map normal $MOD $key set-focused-tags $tags

    # $MOD+Shift+[1-9] to tag focused view with tag [0-8]
    riverctl map normal $MOD+Shift $key set-view-tags $tags

    # $MOD+Control+[1-9] to toggle focus of tag [0-8]
    riverctl map normal $MOD+Control $key toggle-focused-tags $tags

    # $MOD+Shift+Control+[1-9] to toggle tag [0-8] of focused view
    riverctl map normal $MOD+Shift+Control $key toggle-view-tags $tags

    i=$(($i+1))
done

# Iterate over screens (Did my best to try to find all screen)
i=0
for output in $(lswt | grep -w -E "HDMI|DP" | sed 's/.$//' )
do
  key=${keys[i]}

  # $MOD+Alt+[1-9] set focus to screen
  riverctl map normal $MOD+Alt $key focus-output $output

  # $MOD+Shife+Alt+[1-9] set currently focused view to output
  riverctl map normal $MOD+Alt+Shift $key send-to-output $output

  i=$(($i+1))
done

# $MOD+Space to toggle float
riverctl map normal $MOD Space toggle-float

# $MOD+F to toggle fullscreen
riverctl map normal $MOD F toggle-fullscreen

# $MOD+{Up,Right,Down,Left} to change layout orientation
riverctl map normal $MOD Up    send-layout-cmd rivertile "main-location top"
riverctl map normal $MOD Right send-layout-cmd rivertile "main-location right"
riverctl map normal $MOD Down  send-layout-cmd rivertile "main-location bottom"
riverctl map normal $MOD Left  send-layout-cmd rivertile "main-location left"

# Control MPRIS aware media players with playerctl (https://github.com/altdesktop/playerctl)
riverctl map normal None XF86AudioMedia spawn 'playerctl play-pause'
riverctl map normal None XF86AudioPlay  spawn 'playerctl play-pause'
riverctl map normal None XF86AudioPrev  spawn 'playerctl previous'
riverctl map normal None XF86AudioNext  spawn 'playerctl next'

riverctl map normal None XF86MonBrightnessUp   spawn 'xbacklight -inc 10'
riverctl map normal None XF86MonBrightnessDown spawn 'xbacklight -dec 10'

# Various media key mapping examples for both normal and locked mode which do
# not have a modifier
for mode in normal locked
do
    # $MOD+Shift+Q to quit river
    riverctl map $mode $MOD+Shift Q exit

    # Control pulse audio volume with pamixer (https://github.com/cdemoulins/pamixer)
    riverctl map $mode None XF86AudioRaiseVolume  spawn 'pamixer -i 5'
    riverctl map $mode None XF86AudioLowerVolume  spawn 'pamixer -d 5'
    riverctl map $mode None XF86AudioMute         spawn 'pamixer --toggle-mute'
done

# Set keyboard repeat rate
riverctl set-repeat 50 300

### Mouse bindings

# $MOD + Left Mouse Button to move views
riverctl map-pointer normal $MOD BTN_LEFT move-view

# $MOD + Right Mouse Button to resize views
riverctl map-pointer normal $MOD BTN_RIGHT resize-view

### Applications

# Wallpaper
# riverctl spawn "swaybg -i ~/.config/wallpaper.jpg"

# Start foot terminal as a server
riverctl spawn "foot -s"

# File manager (Auto mount usb)
riverctl spawn "pcmanfm -d"

# Notifications
riverctl spawn "mako"

# Bar
WAYCONFIG="~/.config/waybar/river/config"
WAYSTYLE="~/.config/waybar/river/style.css"
riverctl spawn "waybar -c $WAYCONFIG -s $WAYSTYLE"

# Set the default layout generator to be rivertile and start it.
# River will send the process group of the init executable SIGTERM on exit.
riverctl default-layout rivertile
riverctl spawn "rivertile -outer-padding 0 -view-padding 0 -main-ratio 0.5"
