local awful			= require("awful")
local beautiful = require("beautiful")
local naughty		= require("naughty")
local wibox			= require("wibox")
local gears			= require("gears")
local watch			= require("awful.widget.watch")

-- { Volume Widget Settings } -- 

local GET_VOLUME 				= "ponymix get-volume"
local INCREASE_VOLUME 	= "ponymix increase " -- Command need to have space at the end
local DECREASE_VOLUME 	= "ponymix decrease " -- Command need to have space at the ebd
local TOGGLE_VOUME			= "ponymix toggle"
local UPDATE_FREQUENCY 	= 5

-- { Actual code of the widget } --

local volume = {}

-- { Manage the text part of the widget } -- 

volume.text_widget = wibox.widget {
	align 	= "center",
	widget 	= wibox.widget.textbox,
	font		= "AnonymiceProNerdFontMono 13",
	text 		= "100♫",
}

local function update_text_widget(widget, stdout, _, _, _)
		volume_number = string.format("% 3d", stdout)
		widget:set_text(volume_number .. "♫")
end

-- { Manage the control part of the widget } --

function volume.control(cmd, value)
  value = value or 2
	if 			cmd	== "increase" then cmd = INCREASE_VOLUME .. value
	elseif 	cmd == "decrease" then cmd = DECREASE_VOLUME .. value
	elseif 	cmd == "toggle" 	then cmd = TOGGLE_VOMULE
	end
	awful.spawn.easy_async(cmd, function(stdout, stderr, exitreason, exitcode)
			update_text_widget(volume.text_widget, stdout, stderr, exitreason, exitcode)
	end)
end

watch(GET_VOLUME, UPDATE_FREQUENCY, update_text_widget, volume.text_widget)
return volume
