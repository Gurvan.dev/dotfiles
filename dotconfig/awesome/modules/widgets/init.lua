widgets = {
	volume 			= require("modules.widgets.volume"),
	brightness 	= require("modules.widgets.brightness"),
	battery			= require("modules.widgets.battery")
}

return widgets
