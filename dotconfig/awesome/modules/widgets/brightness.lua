local awful			= require("awful")
local beautiful = require("beautiful")
local naughty		= require("naughty")
local wibox			= require("wibox")
local gears			= require("gears")
local watch			= require("awful.widget.watch")

-- { Volume Widget Settings } -- 

local GET_BRIGHTNESS 				= "xbacklight -get"
local INCREASE_BRIGHTNESS 	= "xbacklight -inc "
local DECREASE_BRIGHTNESS 	= "xbacklight -dec "
local UPDATE_FREQUENCY			= 30

local brightness = {}

-- { Manage the text part of the widget } -- 

brightness.text_widget = wibox.widget {
	align 	= "center",
	widget 	= wibox.widget.textbox,
	font		= "AnonymiceProNerdFontMono 11",
	text 		= "[ 100* ]"
}

local function update_text_widget(widget, stdout, _, _, _)
		widget:set_text("[ " .. stdout .. "* ]")
end

-- { Manage the control part of the widget } --

function brightness.control(cmd, value)
  value = value or 2
	if 			cmd == "increase" then cmd = INCREASE_BRIGHTNESS .. value
	elseif 	cmd == "decrease" then cmd = DECREASE_BRIGHTNESS .. value
	end
	awful.spawn.easy_async(cmd, function(stdout, stderr, exitreason, exitcode)
			update_text_widget(brightness.text_widget, stdout, stderr, exitreason, exitcode)
	end)
end

watch(GET_BRIGHTNESS, UPDATE_FREQUENCY, update_text_widget, brightness.text_widget)
return brightness
