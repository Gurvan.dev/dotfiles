local awful			= require("awful")
local beautiful = require("beautiful")
local naughty		= require("naughty")
local wibox			= require("wibox")
local gears			= require("gears")
local watch			= require("awful.widget.watch")

-- { Volume Widget Settings } -- 

local GET_BATTERY				= "acpi -b" 
local UPDATE_FREQUENCY 	= 30
local LOW_THRESHOLD			= 10 
-- TODO : Change color when battery < low_threshold

local battery = {}

-- { Manage the text part of the widget } -- 

battery.text_widget = wibox.widget {
	align 	= "center",
	widget 	= wibox.widget.textbox,
	font		= "AnonymiceProNerdFontMono 13",
	text 		= "100"
}

local function update_text_widget(widget, stdout, _, _, _)
		b, e 		= string.find(stdout, "[0-9]*%%")		-- Find battery percentage
		number	= string.sub(stdout, b, e-1) 				-- Drop the percentage char
		dis     = (string.find(stdout, "Discharging") == Nil)
		if dis then
			charging = '^'
		else 
			charging = 'v'
		end
		widget:set_text(number .. charging) -- "⚡"
end

watch(GET_BATTERY, UPDATE_FREQUENCY, update_text_widget, battery.text_widget)
return battery
