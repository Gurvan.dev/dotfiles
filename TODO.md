# TODO

## Général

* Mettre les image sur un site externe plutôt que sur le répo git pour permettre
  un clonage plus rapide.
* Mettre un lien vers tous les logiciels dans le README
* Mettre un lien vers la licence Anonymice dans le README
* Config zls: désactiver le correct error on save.

## River

* GUI pour NetworkManager a activer sur clic droit du widget
* Désactiver bluetooth sur clic sur widget
* Script pour toggle entre light et dark theme
* Notifications pour la luminosité
* Quand on bouge une fenêtre de moniteur, l'envoyer vers le workspace actif du
  moniteur. (Feature implémentée sur prochaine version de river)
* Mode ne pas déranger pour les notifs

## Neovim

* Trouver un meilleur moyen d'empecher les lsp de overwrite formatoptions et
  d'y ajouter "o".
* Désactiver '*'?

## Pcmanfm

* Ajouter shortcut "ESC" pour revenir en arrière -> Possible ?

## Scripts

* Utiliser un script qui set /etc/environment correctement
  -> QTTheme=qt5ct, Utiliser tout les threads (Pour go notament)
* Séparer les différents package pour offrir l'option de ne pas tous les
  installer en même temps.
