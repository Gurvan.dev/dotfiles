# README

The configuration files for my system!
Features:
* Mostly minimal software
* Installation scripts to easily configure  your system

## Installation

First clone this git repo using:

`` git clone https://gitlab.com/Gurvan.dev/dotfiles --depth 1 ``

*Note: As the installation will make symlink, you must keep the repo where it
was cloned and you shouldn't delete it*

Then run the `setup.sh` script.

The `install_package.sh` script download every needed packages. It currently
only is available:
* On Arch Linux with pacman and/or [yay](https://github.com/Jguer/yay) is
  available
* On Fedora if DNF is available.

Otherwise, you can check the list of required software and install it with your
package manager.

## Softwares

Provide configuration for the following software:
* [River](https://github.com/riverwm/river)
* [Neovim](https://neovim.io)
* [Zsh](https://zsh.org)
* [Foot](https://codeberg.org/dnkl/foot)
* [Alacritty](https://alacritty.org)

## Fonts

* [Anonymice](https://www.nerdfonts.com/)

## River

Usage of river require:

| Software      | Usage              |
|---------------|--------------------|
| rivercarro    | Layout Management  |
| foot          | Terminal           |
| kickoff       | Launcher           |
| pcmanfm       | File explorer      |
| swaylock      | Screen locker      |
| swaybg        | Background         |
| waybar        | Top bar            |
| grim          | Screenshot utility |
| mako          | Notifications      |

## Showcase

![desktop](./imgs/desktop.png)

![nvim](./imgs/nvim.png)

## Credits

Inspirations:
* [PaulPatault/dotfiles](https://github.com/paulpatault/dotfiles)
* [valoranM/dotfiles](https://gitlab.com/valoranM/dotfiles)
