#!/bin/sh
# Author      : gurvan.dev  <https://gitlab.com/gurvan.dev>
# Improved by : valoranM    <https://gitlab.com/valoranM>

BASEDIR=$(realpath $(dirname "$0"))
echo $BASEDIR

echo "Installing dotfiles..."
mkdir -p $HOME/.config

# --------
# arg 1 : Name of config
# arg 2 : Origin path
# arg 3 : Destination path
# --------
install_config ()
{
  if [ -e $3 ]
  then
    echo "Config for $1 exists."
    read -p "Do you want to replace it? [y/N] " yn
    case $yn in
        [Yy]* ) rm -rfv $3 && ln -sfv $2 $3
    esac
  else
    ln -sfv $2 $3
  fi
}

for config in $(ls -A $BASEDIR/dotconfig);
do
  install_config $config $BASEDIR/dotconfig/$config $HOME/.config/$config
done

for homeconfig in $(ls -A $BASEDIR/home);
do
  install_config $homeconfig $BASEDIR/home/$homeconfig $HOME/$homeconfig
done

# Fonts
sudo mkdir -p /usr/share/fonts/OTF/
sudo mkdir -p /usr/share/fonts/TTF/
sudo cp -v $BASEDIR/fonts/*.ttf /usr/share/fonts/TTF/

# Open PDF with zathura by default
xdg-mime default org.pwmt.zathura.desktop application/pdf

echo "Done."
